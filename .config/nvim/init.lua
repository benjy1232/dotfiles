require('plugins')
require('colorscheme')

vim.cmd('set number')

require'lspconfig'.pyright.setup{}
require'lspconfig'.clangd.setup{}
require'lspconfig'.rust_analyzer.setup{}

