local use = require('packer').use

return require('packer').startup(function()
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'

	-- Color Themes
	use '4513ECHO/vim-colors-hatsunemiku'
	use 'catppuccin/nvim'

	-- Airline
	use 'vim-airline/vim-airline'
	use 'vim-airline/vim-airline-themes'

	-- LSP Configurations
	use 'neovim/nvim-lspconfig'

	-- editorconfig
	use 'editorconfig/editorconfig-vim'
end)
