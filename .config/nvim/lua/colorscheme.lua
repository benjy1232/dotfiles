if vim.fn.has('gui_running') == 0 and vim.fn.has('termguicolors') == 1 then
	vim.cmd('set termguicolors')
end

local theme_name = 'catppuccin'

vim.g.airline_theme = theme_name
vim.cmd('colorscheme ' .. theme_name)
